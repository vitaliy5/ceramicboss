$( document ).ready(function() {

    $('header .burger').click(function () {
        $('.wrap__menu').toggleClass('active');
        $('header').toggleClass('active');
        $('body').toggleClass('open-menu');
    });
   
    $('.home .burger').click(function () {
        $('.home__menu__wrap').toggleClass('active');
        $('.home').toggleClass('active');
    });

    if($('.gallery__inner').length) {
        $('.gallery__inner').magnificPopup({
            delegate: 'a', 
            type: 'image',
            gallery:{enabled:true},
            image: {
                titleSrc: 'title' 
            }
        })    
    }
    $('.language select').niceSelect({
			
    });
    $('.home__carousel ').owlCarousel({
        loop: true,
        items:1,     
        autoplay: true,
        autoplayTimeout: 8000,
        dots: true,
        nav: true,
        smartSpeed: 1500,
        navText: [$('.owl-prev'),$('.owl-next')],
        lazyLoad : true

    });


    $(function(){
        var owl = $('.about__gallery');
        owl.owlCarousel({
            loop: false,
            items:3,     
            autoWidth: true,
            dots: false,
            nav: true,
            smartSpeed: 1500,
            margin: 15,
          onInitialized  : counter, 
          onTranslated : counter ,
            responsive : {
            
                0 : {
                    items:3
                },
                
                768 : {
                    items:3   
                }
            }
        });
        
        function counter(event) {
           var element   = event.target;        
            var items     = event.item.count;   
            var item      = event.item.index + 1; 
          
          if(item > items) {
            item = item - items
          }
          $('.about__counter').html(""+item+" / "+items)
        }
    });
    
});